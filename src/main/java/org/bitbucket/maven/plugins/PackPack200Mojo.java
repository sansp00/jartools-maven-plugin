package org.bitbucket.maven.plugins;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.maven.model.FileSet;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name = "pack", requiresProject = true)
public class PackPack200Mojo extends AbstractJarMojo {
    /**
     * File set to which the entries are applied
     */
    @Parameter(required = true)
    protected FileSet fileSet;

    @Parameter(required = false, defaultValue = "gzip")
    protected String outputType;

    @Parameter(required = false, defaultValue = "true")
    protected Boolean pruneSourceArchive;

    private static final int NTHREDS = 10;

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.maven.plugin.Mojo#execute()
     */

    public void execute() throws MojoExecutionException {
        ExecutorService executor = Executors.newFixedThreadPool(NTHREDS);
        List<Future<Integer>> list = new ArrayList<Future<Integer>>();
        List<File> archives = getFileList(fileSet, true);

        for (File archive : archives)
        {
            CallablePack worker = new CallablePack(archive, outputType, pruneSourceArchive);
            Future<Integer> submit = executor.submit(worker);
            list.add(submit);
        }

        try
        {
            executor.shutdown();
            executor.awaitTermination(30, TimeUnit.MINUTES);
        } catch (InterruptedException e)
        {
            getLog().error("Timed out while packing", e);
            throw new MojoExecutionException("Timed out while packing", e);
        }

    }

    public class CallablePack implements Callable<Integer> {
        File archive;
        String outputType;
        Boolean pruneSourceArchive;

        public CallablePack(File archive, String outputType, Boolean pruneSourceArchive) {
            this.archive = archive;
            this.outputType = outputType;
            this.pruneSourceArchive = pruneSourceArchive;
        }

        @Override
        public Integer call() throws Exception {
            try
            {
                getLog().info("Packing archive " + archive.getName());
                pack(archive, outputType, pruneSourceArchive);
            } catch (IOException e)
            {
                getLog().error("Exception while packing: " + archive.getName(), e);
                throw new MojoExecutionException("Exception while packing: " + archive.getName(), e);
            }
            return 0;
        }
    }

}
