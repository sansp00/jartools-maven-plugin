package org.bitbucket.maven.plugins;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.maven.model.FileSet;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name = "manifest", requiresProject = true)
public class ManifestMojo extends AbstractJarMojo {
    /**
     * File set to which the entries are applied
     */
    @Parameter(required = true)
    protected FileSet fileSet;

    /**
     * File set of jar entry to add
     */
    @Parameter(required = false)
    protected FileSet jarEntrySet;

    /**
     * Main attributes to add
     */
    @Parameter
    protected Map<String, String> mainAttributes;

    /**
     * Extra attributes to add
     */
    @Parameter
    protected Map<String, Map<String, String>> manifestEntries;

    /**
     * Remove signature
     */
    @Parameter(defaultValue = "false")
    protected Boolean pruneSignature;

    /**
     * Remove signature
     */
    @Parameter(defaultValue = "false")
    protected Boolean generateSignature;

    /**
     * Force operation
     */
    @Parameter(defaultValue = "false")
    protected Boolean force;

    /**
     * Remove INDEX.LIST
     */
    @Parameter(defaultValue = "false")
    protected Boolean pruneIndex;

    private static final int NTHREDS = 10;

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.maven.plugin.Mojo#execute()
     */

    public void execute() throws MojoExecutionException {
        ExecutorService executor = Executors.newFixedThreadPool(NTHREDS);
        List<Future<Integer>> list = new ArrayList<Future<Integer>>();
        List<File> archives = getFileList(fileSet, true);

        for (File archive : archives)
        {
            CallableUpdateManifest worker = new CallableUpdateManifest(archive, jarEntrySet, mainAttributes, manifestEntries, pruneSignature, generateSignature,
                    pruneIndex, force);
            Future<Integer> submit = executor.submit(worker);
            list.add(submit);
        }

        try
        {
            executor.shutdown();
            executor.awaitTermination(30, TimeUnit.MINUTES);
        } catch (InterruptedException e)
        {
            getLog().error("Timed out while updating", e);
            throw new MojoExecutionException("Timed out while updating", e);
        }

    }

    public class CallableUpdateManifest implements Callable<Integer> {
        File archive;
        FileSet jarEntrySet;
        Map<String, String> mainAttributes;
        Map<String, Map<String, String>> manifestEntries;
        Boolean pruneSignature;
        Boolean generateSignature;
        Boolean pruneIndex;
        Boolean force;

        public CallableUpdateManifest(File archive, //
                FileSet jarEntrySet, Map<String, String> mainAttributes, //
                Map<String, Map<String, String>> manifestEntries, //
                Boolean pruneSignature, Boolean generateSignature, //
                Boolean pruneIndex, //
                Boolean force) {
            this.archive = archive;
            this.jarEntrySet = jarEntrySet;
            this.mainAttributes = mainAttributes;
            this.manifestEntries = manifestEntries;
            this.pruneSignature = pruneSignature;
            this.generateSignature = generateSignature;
            this.pruneIndex = pruneIndex;
            this.force = force;
        }

        @Override
        public Integer call() throws Exception {
            try
            {
                getLog().info("Updating archive " + archive.getName());
                updateManifest(archive, jarEntrySet, mainAttributes, manifestEntries, pruneSignature, generateSignature, pruneIndex, force);
            } catch (Exception e)
            {
                getLog().error("Exception while updating: " + archive.getName(), e);
                throw new MojoExecutionException("Exception while updating: " + archive.getName(), e);
            }
            return 0;
        }
    }

}
