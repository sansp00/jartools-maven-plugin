package org.bitbucket.maven.plugins;

public class ManifestAttributeKeys {
	public static class GeneralMainAttributes {
		static String ManifestVersion = "Manifest-Version";
		static String CreatedBy = "Created-By";
		static String SignatureVersion = "Signature-Version";
		static String ClassPath = "Class-Path";
		static String MainClass = "Main-Class";

	}

	public static class AppletsAttributes {
		static String ExtensionList = "Extension-List";
		static String ExtensionName = "-Extension-Name";
		static String SpecificationVersion = "-Specification-Version";
		static String ImplementationVersion = "-Implementation-Version";
		static String ImplementationVendorId = "-Implementation-Vendor-Id";
		static String ImplementationURL = "-Implementation-URL";
	}

	public static class ExtenstionIdentificationAttributes {
		static String ExtensionName = "Extension-Name";
	}

	public static class PackageVersioningAndSealingAttributes {
		static String ImplementationTitle = "Implementation-Title";
		static String ImplementationVersion = "Implementation-Version";
		static String ImplementationVendor = "Implementation-Vendor";
		static String ImplementationVendorId = "Implementation-Vendor-Id";
		static String ImplementationURL = "Implementation-URL";
		static String SpecificationTitle = "Specification-Title";
		static String SpecificationVersion = "Specification-Version";
		static String SpecificationVendor = "Specification-Vendor";
		static String Sealed = "Sealed";
	}
}
