package org.bitbucket.maven.plugins;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.model.FileSet;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import com.interfacing.security.tools.Argument;

@Mojo(name = "sign", requiresProject = true)
public class SignMojo extends AbstractJarMojo {
    /**
     * File set to which the entries are applied
     */
    @Parameter(required = true)
    protected FileSet fileSet;

    @Argument(name = "keystore")
    @Parameter(required = true)
    String keystore; // Specifies the URL that tells the keystore location.

    @Argument(name = "storetype")
    @Parameter(required = false)
    String keystoreType; // Specifies the type of keystore to be instantiated.

    @Argument(name = "storepass")
    @Parameter(required = true)
    String keystorePassword; // Specifies the password which is required to access the keystore.

    @Argument(name = "keypass")
    @Parameter(required = false)
    String keystoreKeyPassword; // Specifies the password used to protect the private key of the keystore entry addressed by the alias specified on the command
                                // line.

    @Argument(name = "sigfile")
    @Parameter(required = false)
    String signatureFile; // Specifies the base file name to be used for the generated .SF and .DSA files.

    @Argument(name = "sigalg")
    @Parameter(required = false)
    String signatureAlgorithm; // Specifies the name of the signature algorithm to use to sign the JAR file.

    @Argument(name = "digestalg")
    @Parameter(required = false)
    String digestAlgorithm; // Specifies the name of the message digest algorithm to use when digesting the entries of a jar file.

    @Argument(name = "provider")
    @Parameter(required = false)
    String provider; // Used to specify the name of cryptographic service provider's master class file when the service provider is not listed in the security
                     // properties file, java.security

    @Argument(name = "provider")
    @Parameter(required = false)
    String providerClassName; // Used to specify the name of cryptographic service provider's master class file when the service provider is not listed in the
                              // security properties file, java.security

    @Argument(name = "providerName")
    @Parameter(required = false)
    String providerName; // If more than one provider has been configured in the java.security security properties file, you can use the -providerName option to
                         // target a specific provider instance

    @Argument(name = "tsa")
    @Parameter(required = false)
    String timestampSignature; // If "-tsa http://example.tsa.url" appears on the command line when signing a JAR file then a timestamp is generated for the
                               // signature.

    @Argument(name = "tsacert")
    @Parameter(required = false)
    String timestampSignatureCertificateAlias; // If "-tsacert alias" appears on the command line when signing a JAR file then a timestamp is generated for the
                                               // signature.

    @Argument(name = "altsigner")
    @Parameter(required = false)
    String alternateSigner; // Specifies that an alternative signing mechanism be used.

    @Argument(name = "altsignerpath")
    @Parameter(required = false)
    String alternateSignerPath; // Specifies the path to the class file (the class file name is specified with the -altsigner option described above) and any
                                // JAR files it depends on.

    @Argument(name = "debug", toggle = true)
    @Parameter(required = false)
    Boolean debug; // debug

    @Argument(name = "verbose", toggle = true)
    @Parameter(required = false)
    Boolean verbose; // verbose

    @Parameter(required = false)
    String alias; // alias

    private static final int NTHREDS = 10;

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.maven.plugin.Mojo#execute()
     */

    public void execute() throws MojoExecutionException {
        ExecutorService executor = Executors.newFixedThreadPool(NTHREDS);
        List<Future<Integer>> list = new ArrayList<Future<Integer>>();
        List<File> archives = getFileList(fileSet, true);

        PrintStream originalSystemOut = System.out;
        PrintStream originalSystemErr = System.err;
        PrintStream interceptorOut = new Interceptor(originalSystemOut, InterceptorLogLevelEnum.debug);
        PrintStream interceptorErr = new Interceptor(originalSystemErr, InterceptorLogLevelEnum.error);

        System.setOut(interceptorOut);// just add the interceptor
        System.setErr(interceptorErr);// just add the interceptor

        try
        {
            List<String> arguments = getArguments(this);

            for (File archive : archives)
            {
                CallableSign worker = new CallableSign(archive, alias, arguments);
                Future<Integer> submit = executor.submit(worker);
                list.add(submit);

            }
        } catch (IllegalArgumentException | IllegalAccessException e)
        {
            getLog().error("Exception while initiating signing", e);
            throw new MojoExecutionException("Exception while initiating signing", e);
        }

        try
        {
            executor.shutdown();
            executor.awaitTermination(30, TimeUnit.MINUTES);
        } catch (InterruptedException e)
        {
            getLog().error("Timed out while signing", e);
            throw new MojoExecutionException("Timed out while signing", e);
        } finally
        {
            System.setOut(originalSystemOut);// just add the interceptor
            System.setErr(originalSystemErr);// just add the interceptor
        }

    }

    public class CallableSign implements Callable<Integer> {
        List<String> arguments;
        sun.security.tools.JarSigner signer;
        File archive;
        String alias;

        public CallableSign(File archive, String alias, List<String> arguments) {
            this.archive = archive;
            this.alias = alias;
            this.arguments = arguments;
        }

        @Override
        public Integer call() throws Exception {
            try
            {
                getLog().info("Signing archive " + archive.getName());

                // Clone the argument list
                List<String> args = new ArrayList<>();
                args.addAll(arguments);

                // Add the file argument
                args.add(archive.getAbsolutePath());

                if (StringUtils.isNotBlank(alias))
                {
                    // Add the alias argument
                    args.add(alias);
                }

                StringBuffer strbuf = new StringBuffer();
                for (String arg : args)
                {
                    strbuf.append(arg).append(" ");
                }

                getLog().debug("Signing archive with args: " + strbuf.toString());

                try
                {
                    // Spawn the signer
                    signer = new sun.security.tools.JarSigner();

                    // Run the signer
                    signer.run(args.toArray(new String[args.size()]));

                } catch (Throwable e)
                {
                    getLog().error("Throwable while signing: " + archive.getName(), e);
                    throw new MojoExecutionException("Throwable while signing: " + archive.getName(), e);
                }
            } catch (Exception e)
            {
                getLog().error("Exception while signing: " + archive.getName(), e);
                throw new MojoExecutionException("Exception while signing: " + archive.getName(), e);
            }
            return 0;
        }
    }
}
