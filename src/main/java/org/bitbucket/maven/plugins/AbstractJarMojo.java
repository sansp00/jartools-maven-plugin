package org.bitbucket.maven.plugins;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.jar.Attributes;
import java.util.jar.Attributes.Name;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.jar.Pack200;
import java.util.jar.Pack200.Packer;
import java.util.jar.Pack200.Unpacker;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.model.FileSet;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.codehaus.plexus.util.FileUtils;

import com.google.common.base.Preconditions;
import com.interfacing.security.tools.Argument;

public abstract class AbstractJarMojo extends AbstractMojo {

    protected static Unpacker unpacker = Pack200.newUnpacker();
    protected static Packer packer = Pack200.newPacker();

    protected static final String ManifestFileName = "META-INF/MANIFEST.MF";
    protected static final String IndexFileName = "META-INF/INDEX.LIST";
    protected static final String TempExtension = ".tmp";

    protected static final String DigestAlgorithm = "SHA";
    protected static final String SignatureFileExtension = ".SF";

    protected static final String RSA_DigitalSignatureFileExtension = ".RSA";
    protected static final String DSA_DigitalSignatureFileExtension = ".DSA";
    protected static final String PGP_DigitalSignatureFileExtension = ".PGP";
    protected static final String PackExtension = ".pack";
    protected static final String GZipExtension = ".gz";
    protected static final String JarExtension = ".jar";

    protected enum PackOutputType {
        jar, pack, gzip;
    }

    /**
     * Update a manifest
     * 
     * @param archive
     * @param jarEntries
     * @throws IOException
     * @throws MojoExecutionException
     */
    protected void updateManifest(File archive, //
            FileSet jarEntrySet, //
            Map<String, String> mainAttributes, //
            Map<String, Map<String, String>> manifestEntries, //
            Boolean pruneSignature, Boolean generateSignature, //
            Boolean pruneIndex, //
            Boolean force) throws IOException, MojoExecutionException {
        File tempFile = File.createTempFile(generateTempName(getJarBasename(archive)), TempExtension);
        tempFile.deleteOnExit();

        getLog().debug("Created temporary archive " + tempFile.getAbsolutePath());

        try (JarFile jar = new JarFile(archive.getAbsolutePath()))
        {
            getLog().debug("Processing archive for " + archive.getName());

            Manifest manifest = null;
            try
            {
                manifest = jar.getManifest();
                if (manifest == null)
                {
                    getLog().debug("Generating manifest for archive " + archive.getName());
                    manifest = generateDefaultManifest();
                }
            } catch (IOException e)
            {
                getLog().error("Unable to read manifest of archive " + archive.getName(), e);
                return;
            }

            if (containsSignature(manifest) && !pruneSignature && !force)
            {
                getLog().error("Manifest contains signature, please prune");
                return;
            }

            if (containsSignature(manifest) && pruneSignature)
            {
                getLog().debug("Pruning Manifest signature");
                pruneSignature(manifest);
            }

            if (hasMainAttributes(mainAttributes))
            {
                getLog().debug("Adding new main attributes to Manifest");
                addMainAttributesToManifest(manifest, mainAttributes);
            }

            if (hasManifestEntries(manifestEntries))
            {
                getLog().debug("Adding new main attributes entries to Manifest");
                addEntrieToManifest(manifest, manifestEntries);
            }

            logManifest(manifest);

            getLog().debug("Opening temporary archive " + tempFile.getName());

            try (JarOutputStream jos = new JarOutputStream(new FileOutputStream(tempFile), manifest))
            {
                getLog().debug("Copying Jar entries");
                copyJarEntries(jar, jos, pruneSignature, pruneIndex);
                if (jarEntrySet != null)
                {
                    List<File> jarEntries = getFileList(jarEntrySet, true);
                    if (hasJarEntries(jarEntries))
                    {
                        getLog().debug("Adding new Jar entries");
                        addJarEntries(jarEntries, jarEntrySet, jos);
                    }
                }
            } catch (IOException e)
            {
                getLog().error("Unable to open temporary file for processing", e);
                return;
            }
        } catch (IOException | SecurityException e)
        {
            getLog().error("Unable to open archive " + archive.getName(), e);
            return;
        }
        getLog().debug("Replacing original archive " + archive.getName());
        try
        {
            FileUtils.rename(tempFile, archive);
        } catch (IOException e)
        {
            getLog().error("Unable to replace archive " + archive.getName(), e);
        }
    }

    /**
     * Indicates if has configured attributes
     * 
     * @return
     */
    protected boolean hasJarEntries(List<File> jarEntries) {
        return jarEntries != null && !jarEntries.isEmpty();
    }

    /**
     * Indicates if has configured attributes
     * 
     * @return
     */
    protected boolean hasMainAttributes(Map<String, String> mainAttributes) {
        return mainAttributes != null && !mainAttributes.isEmpty();
    }

    /**
     * Indicates if has configured entries
     * 
     * @return
     */
    protected boolean hasManifestEntries(Map<String, Map<String, String>> manifestEntries) {
        return manifestEntries != null && !manifestEntries.isEmpty();
    }

    /**
     * @param jar
     * @param outputTypeString
     * @param pruneSource
     * @throws IOException
     */
    protected void pack(File jar, String outputTypeString, Boolean pruneSourceArchive) throws IOException {
        pack(jar, PackOutputType.valueOf(outputTypeString), pruneSourceArchive);
    }

    /**
     * Pack a file
     * 
     * @param jar
     * @throws IOException
     */
    protected void pack(File jar, PackOutputType outputType, Boolean pruneSourceArchive) throws IOException {
        Preconditions.checkArgument(jar.exists());
        Preconditions.checkArgument(jar.canRead());
        Preconditions.checkArgument(jar.canWrite());
        Preconditions.checkArgument(FileUtils.getExtension(jar.getAbsolutePath()).equalsIgnoreCase("jar"));

        // Create temp file to do dirty work
        File tempFile = File.createTempFile(generateTempName(getJarBasename(jar)), TempExtension);
        tempFile.deleteOnExit();
        getLog().debug("Created temporary archive " + tempFile.getAbsolutePath());

        // Pack and gzip to temp file
        try (OutputStream out = outputType == PackOutputType.gzip ? new GZIPOutputStream(new FileOutputStream(tempFile)) : new FileOutputStream(tempFile);)
        {
            JarFile jarFile = new JarFile(jar);
            getLog().debug(
                    "Packing " + (outputType == PackOutputType.gzip ? "and GZipping " : " ") + jar.getAbsolutePath() + " to temporary archive " + tempFile
                            .getAbsolutePath());
            packer.pack(jarFile, out);
        }

        switch (outputType)
        {
            case gzip:
                // Add .pack.gz archive to normal jar
                getLog().debug("Adding gzipped packed archive (.pack.gz) of " + jar.getAbsolutePath());

                Path gzipPackFilePath = Paths.get(getJarFilename(jar) + PackExtension + GZipExtension);
                Files.createFile(gzipPackFilePath);
                FileUtils.copyFile(tempFile, gzipPackFilePath.toFile());
                if (pruneSourceArchive)
                {
                    getLog().debug("Pruning source archive " + jar.getAbsolutePath());
                    FileUtils.fileDelete(jar.getAbsolutePath());
                }
                break;
            case pack:
                // Add packed version of jar
                getLog().debug("Adding packed archive (.pack) of " + jar.getAbsolutePath());
                Path packFilePath = Paths.get(jar.getAbsolutePath() + PackExtension);
                Files.createFile(packFilePath);
                FileUtils.copyFile(tempFile, packFilePath.toFile());
                if (pruneSourceArchive)
                {
                    getLog().debug("Pruning source archive " + jar.getAbsolutePath());
                    FileUtils.fileDelete(jar.getAbsolutePath());
                }
                break;
            case jar:
                // Replace archive with packed one
                getLog().debug("Overwriting source archive with packed archive " + jar.getAbsolutePath());
                FileUtils.copyFile(tempFile, jar);
            default:
                break;
        }
    }

    /**
     * Unpack a file
     * 
     * @param jar
     * @throws IOException
     */
    protected void unpack(File jar) throws IOException {
        Preconditions.checkArgument(jar.exists());
        Preconditions.checkArgument(jar.canRead());
        Preconditions.checkArgument(jar.canWrite());

        // Create temp file to do dirty work
        File tempFile = File.createTempFile(generateTempName(getJarBasename(jar)), TempExtension);
        tempFile.deleteOnExit();
        getLog().debug("Created temporary archive" + tempFile.getAbsolutePath());

        Boolean gzipped = isGZippedArchive(jar);
        Boolean packed = isPackedArchive(jar);

        try (JarOutputStream out = gzipped ? new JarOutputStream(new GZIPOutputStream(new FileOutputStream(tempFile))) : new JarOutputStream(
                new FileOutputStream(tempFile));)
        {
            getLog().debug("Unpacking " + (gzipped ? "GZipped " : " ") + "to temporary archive " + tempFile.getAbsolutePath());
            unpacker.unpack(jar, out);
        }

        if (gzipped || packed)
        {
            // Add normal unpacked jar to .pack and/or .gz archived
            getLog().debug("Adding unpacked archive of " + jar.getAbsolutePath());
            Path jarFilePath = Paths.get(getJarFilename(jar));
            Files.createFile(jarFilePath);
            FileUtils.copyFile(tempFile, jarFilePath.toFile());
        } else
        {
            // Replace archive with unpacked one
            getLog().debug("Overwriting with unpacked archive " + jar.getAbsolutePath());
            FileUtils.copyFile(tempFile, jar);
        }
    }

    protected String getJarFilename(File jar) {
        String filename = jar.getAbsolutePath();
        FilenameUtils.getBaseName(filename);
        FilenameUtils.getFullPath(filename);
        return FilenameUtils.getFullPath(filename) + FilenameUtils.getBaseName(filename) + JarExtension;
    }

    protected String getJarBasename(File jar) {
        String filename = jar.getAbsolutePath();
        return FilenameUtils.getBaseName(filename);
    }

    protected String generateTempName(String jarBasename) {
        return String.format("%s_%d", jarBasename, System.currentTimeMillis());
    }

    protected boolean isPackedArchive(File jar) {
        return jar.getName().endsWith(PackExtension) || jar.getName().endsWith(PackExtension + GZipExtension);
    }

    protected boolean isGZippedArchive(File jar) {
        return jar.getName().endsWith(GZipExtension);
    }

    /**
     * @param jar
     * @throws IOException
     */
    protected void repack(File jar) throws IOException {
        pack(jar, PackOutputType.jar, false);
        unpack(jar);
    }

    /**
     * Indicates if the manifest has signature entries
     * 
     * @param manifest
     * @return
     */
    protected boolean containsSignature(Manifest manifest) {
        if (manifest == null)
        {
            return false;
        }

        Map<String, Attributes> currentEntries = manifest.getEntries();
        for (Entry<String, Attributes> manifestEntry : currentEntries.entrySet())
        {
            for (Entry<Object, Object> entry : manifestEntry.getValue().entrySet())
            {
                if (entry.getKey().toString().startsWith(DigestAlgorithm))
                {
                    return true;
                }
            }
        }
        return false;
    }

    protected void logManifest(Manifest manifest) {

        getLog().debug("-- Manifest main attributes start ---");
        Attributes mainAttributes = manifest.getMainAttributes();
        for (Entry<Object, Object> mainAttribute : mainAttributes.entrySet())
        {
            getLog().debug(mainAttribute.getKey() + "-" + mainAttribute.getValue());
        }
        getLog().debug("-- Manifest main attributes end ---");

        Map<String, Attributes> currentEntries = manifest.getEntries();
        getLog().debug("-- Manifest entries start ---");
        for (Entry<String, Attributes> manifestEntry : currentEntries.entrySet())
        {
            for (Entry<Object, Object> entry : manifestEntry.getValue().entrySet())
            {
                getLog().debug(entry.getKey() + ": " + entry.getValue());
            }
        }
        getLog().debug("-- Manifest entries end ---");

    }

    /**
     * Prunes the signature entries from the manifest
     * 
     * @param manifest
     */
    protected void pruneSignature(Manifest manifest) {
        Map<String, Attributes> entries = manifest.getEntries();
        List<String> toRemove = new ArrayList<>();
        for (Entry<String, Attributes> manifestEntry : entries.entrySet())
        {
            for (Entry<Object, Object> entry : manifestEntry.getValue().entrySet())
            {
                if (entry.getKey().toString().startsWith(DigestAlgorithm))
                {
                    toRemove.add(manifestEntry.getKey());
                }
            }
        }
        getLog().debug("Pruning Manifest signature entries");
        for (String key : toRemove)
        {
            entries.remove(key);
        }
    }

    /**
     * Add configured main attributes to manifest
     * 
     * @param manifest
     */
    protected void addMainAttributesToManifest(Manifest manifest, Map<String, String> mainAttributes) {
        Attributes main = manifest.getMainAttributes();
        // This is a mandatory attributes, without it, the manifest does not get written
        if (main.isEmpty())
        {
            main.put(Attributes.Name.MANIFEST_VERSION, "1.0");
        }

        for (Entry<String, String> entry : mainAttributes.entrySet())
        {
            main.put(new Name(entry.getKey()), entry.getValue());
        }

    }

    /**
     * Add configured entries to manifest
     * 
     * @param manifest
     */
    protected void addEntrieToManifest(Manifest manifest, Map<String, Map<String, String>> manifestEntries) {
        Map<String, Attributes> currentEntries = manifest.getEntries();

        for (Entry<String, Map<String, String>> manifestEntry : manifestEntries.entrySet())
        {
            Attributes currentAttributes = null;
            if (currentEntries.containsKey(manifestEntry.getKey()))
            {
                getLog().debug("Appending attributes to existing manifest entry " + manifestEntry.getKey());
                currentAttributes = currentEntries.get(manifestEntry.getKey());
            } else
            {
                currentAttributes = new Attributes();
                currentEntries.put(manifestEntry.getKey(), currentAttributes);
            }

            for (Entry<String, String> attributeEntry : manifestEntry.getValue().entrySet())
            {
                if (currentAttributes.containsKey(new Name(attributeEntry.getKey())))
                {
                    getLog().debug("Replacing attribute in existing manifest entry " + manifestEntry.getKey() + ", " + attributeEntry.getKey());
                }
                currentAttributes.put(new Name(attributeEntry.getKey()), attributeEntry.getValue());
            }
        }
    }

    /**
     * Returns the filtered file set
     * 
     * @return List of file to process
     * @throws MojoExecutionException
     */
    protected List<File> getFileList(FileSet fileSet, Boolean includeBasedir) throws MojoExecutionException {
        try
        {
            File directory = new File(fileSet.getDirectory());
            String includes = getCommaSeparatedList(fileSet.getIncludes());
            String excludes = getCommaSeparatedList(fileSet.getExcludes());
            @SuppressWarnings("unchecked")
            List<File> files = FileUtils.getFiles(directory, includes, excludes, includeBasedir);
            return files;
        } catch (IOException e)
        {
            throw new MojoExecutionException("Unable to get files to process", e);
        }
    }

    /**
     * Returns a comma separated string representation of the list of strings
     * 
     * @param list
     * @return
     */
    protected String getCommaSeparatedList(List<String> list) {
        StringBuffer buffer = new StringBuffer();
        for (Iterator<String> iterator = list.iterator(); iterator.hasNext();)
        {
            String element = iterator.next();
            buffer.append(element);
            if (iterator.hasNext())
            {
                buffer.append(",");
            }
        }
        return buffer.toString();
    }

    /**
     * Copies the source entry into the jar
     * 
     * @param source
     *            file source to copy
     * @param jos
     *            Jar output stream destination
     * @throws IOException
     */
    protected void copySourceEntry(File source, JarOutputStream jos) throws IOException {
        byte[] buffer = new byte[1024];
        int bytesRead;

        FileInputStream file = new FileInputStream(source);

        try
        {
            JarEntry entry = new JarEntry(source.getName());
            jos.putNextEntry(entry);

            while ((bytesRead = file.read(buffer)) != -1)
            {
                jos.write(buffer, 0, bytesRead);
            }

        } finally
        {
            file.close();
        }
    }

    /**
     * Copies jar entries
     * 
     * @param jar
     *            Jar file source
     * @param jos
     *            Jar output stream destination
     * @param pruneSignatures
     *            indicates to prune or not signature files
     * @throws IOException
     */
    protected void copyJarEntries(JarFile jar, JarOutputStream jos, Boolean pruneSignatures, Boolean pruneIndex) throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;

        for (Enumeration<JarEntry> entries = jar.entries(); entries.hasMoreElements();)
        {
            // Get the next entry.

            JarEntry entry = new JarEntry(entries.nextElement().getName());

            if (pruneSignatures && isSignatureExtension(entry.getName()))
            {
                getLog().debug("Pruning signature jar entry");
                continue;
            }

            if (pruneIndex && (entry.getName().endsWith(IndexFileName)))
            {
                getLog().debug("Pruning INDEX.LIST jar entry");
                continue;
            }

            // If the entry has not been added already, add it.
            if (!entry.getName().equals(jar.getName()) && !entry.getName().equals(ManifestFileName))
            {
                // Get an input stream for the entry.

                InputStream entryStream = jar.getInputStream(entry);

                // Read the entry and write it to the temp jar.
                int compressionMethod = entry.getMethod() == -1 ? ZipEntry.DEFLATED : entry.getMethod();

                jos.setMethod(compressionMethod);
                jos.putNextEntry(entry);

                while ((bytesRead = entryStream.read(buffer)) != -1)
                {
                    jos.write(buffer, 0, bytesRead);
                }
                entryStream.close();
            }
        }
    }

    /**
     * Add jar entries
     * 
     * @param jarEntrySet
     * 
     * @param jarEntries
     *            Jar entries to add
     * @param jos
     *            Jar output stream destination
     * @param pruneSignatures
     *            indicates to prune or not signature files
     * @throws IOException
     */
    protected void addJarEntries(List<File> entries, FileSet jarEntrySet, JarOutputStream jos) throws IOException {
        byte[] buffer = new byte[1024];
        int bytesRead;

        for (File entry : entries)
        {
            File directory = new File(jarEntrySet.getDirectory());
            String unFormattedJarEntryName = StringUtils.substringAfter(entry.getPath(), directory.getName());
            String jarEntryName = formatJarEntryName(unFormattedJarEntryName);
            getLog().debug("Adding jar entry: " + jarEntryName + " using content: " + entry.getAbsoluteFile());
            JarEntry jarEntry = new JarEntry(jarEntryName);
            jos.putNextEntry(jarEntry);
            try (InputStream entryStream = new BufferedInputStream(new FileInputStream(entry.getAbsoluteFile())))
            {
                while ((bytesRead = entryStream.read(buffer)) != -1)
                {
                    jos.write(buffer, 0, bytesRead);
                }
            }
        }
    }

    private String formatJarEntryName(String jarEntryName) {
        String result = jarEntryName;
        result = StringUtils.replace(result, "\\\\", "/");
        result = StringUtils.replace(result, "\\", "/");
        result = StringUtils.removeStart(result, "/");
        return result;
    }

    protected boolean isSignatureExtension(String name) {
        return name.endsWith(RSA_DigitalSignatureFileExtension) //
                || name.endsWith(DSA_DigitalSignatureFileExtension) //
                || name.endsWith(PGP_DigitalSignatureFileExtension) //
                || name.endsWith(SignatureFileExtension);
    }

    protected Manifest generateDefaultManifest() {
        Manifest m = new Manifest();
        m.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
        Map<String, String> ma = new HashMap<>();
        ma.put(ManifestAttributeKeys.GeneralMainAttributes.CreatedBy, "Maven JarTools");
        addMainAttributesToManifest(m, ma);

        return m;
    }

    protected List<String> getArguments(Object obj) throws IllegalArgumentException, IllegalAccessException {
        List<String> arguments = new ArrayList<>();
        for (Field field : obj.getClass().getDeclaredFields())
        {
            if (field.getAnnotation(Argument.class) != null)
            {
                Argument arg = field.getAnnotation(Argument.class);
                if (!arg.toggle())
                {
                    Object value = field.get(obj);
                    if (value != null)
                    {
                        arguments.add("-" + arg.name());
                        arguments.add(value.toString());
                    }
                } else
                {
                    if (field.get(obj) != null)
                    {
                        arguments.add("-" + arg.name());
                    }
                }
            }
        }
        return arguments;
    }

    protected enum InterceptorLogLevelEnum {
        error, info, debug;
    }

    protected class Interceptor extends PrintStream {
        InterceptorLogLevelEnum logLevel;

        public Interceptor(OutputStream out, InterceptorLogLevelEnum logLevel) {
            super(out, true);
            this.logLevel = logLevel;
        }

        @Override
        public void println() {
        }

        @Override
        public void print(String s) {
            String str = StringUtils.trimToEmpty(s);
            if (StringUtils.isBlank(str))
            {
                return;
            }
            switch (logLevel)
            {
                case debug:
                    getLog().debug(str);
                    break;
                case error:
                    getLog().error(str);
                    break;
                case info:
                    getLog().info(str);
                    break;
                default:
            }
        }

        @Override
        public void println(String s) {
            String str = StringUtils.trimToEmpty(s);
            if (StringUtils.isBlank(str))
            {
                return;
            }
            switch (logLevel)
            {
                case debug:
                    getLog().debug(str);
                    break;
                case error:
                    getLog().error(str);
                    break;
                case info:
                    getLog().info(str);
                    break;
                default:
            }
        }

    }
}
